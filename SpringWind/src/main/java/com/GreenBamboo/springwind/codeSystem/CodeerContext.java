package com.GreenBamboo.springwind.codeSystem;

/**
 *
 * @author bing <503718696@qq.com>
 * @date 2016-5-15 23:46:16
 * @version v0.1
 */
public class CodeerContext implements Codeer {

    private Codeer codeer;

    public CodeerContext() {
        codeer = new DefaultCodeer();
    }

    public CodeerContext(Codeer codeer) {
        this.codeer = codeer;
    }

    public CodeerContext(String CODEER_NAME) {
        switch (CODEER_NAME) {
            case ACodeer.CODEER_NAME:
                codeer = new ACodeer();
                break;
            case BCodeer.CODEER_NAME:
                codeer = new BCodeer();
                break;
            case CCodeer.CODEER_NAME:
                codeer = new CCodeer();
                break;
            default:
                codeer = new DefaultCodeer();
        }
    }

    public Codeer getCodeer() {
        return codeer;
    }

    public void setCodeer(Codeer codeer) {
        this.codeer = codeer;
    }

    @Override
    public ImageCode getCaptcha() {
        return codeer.getCaptcha();
    }

    @Override
    public boolean checkCaptcha(String codeKey, String clientCode) {
        return codeer.checkCaptcha(codeKey, clientCode);
    }

}
