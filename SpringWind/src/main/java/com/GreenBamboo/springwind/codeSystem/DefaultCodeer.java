package com.GreenBamboo.springwind.codeSystem;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

/**
 * 验证码创建者默认实体类。该类是一个未完成的实现。请后续开发者完善。
 *
 * @author bing <503718696@qq.com>
 * @date 2016-5-17 10:06:32
 * @version
 */
public class DefaultCodeer extends AbstractCodeer {

    public final static String CODEER_NAME = "DefaultCodeer";
    private String randString = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";//用于生产的母字符串
    private Font font = new Font("Fixedsys", Font.CENTER_BASELINE, 18);//验证码字体

    @Override
    public String generateCaptchaString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < codeLength; i++) {
            sb.append(randString.charAt(random.nextInt(randString.length())));
        }
        return sb.toString();
    }

    @Override
    public Image generateCaptchaImage(String code) {
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
        Graphics g = bi.getGraphics();
        g.fillRect(0, 0, width, height);
        g.setColor(ColorUtil.randomColor());
        //绘制干扰线
        drawLine(g);
        //绘制随机字符
        drawCodeString(g, code);
        g.dispose();
        return bi;
    }

    /**
     * 绘制字符串。
     *
     * @param g
     * @param code 随机字符串
     * @param i
     * @return
     */
    private void drawCodeString(Graphics g, String code) {
        g.setFont(font);
        for (int i = 0; i < codeLength; i++) {
            g.translate(random.nextInt(3), random.nextInt(3));
            g.setColor(ColorUtil.randomColor());
            g.drawString(String.valueOf(code.charAt(i)), 13 * i, 16);
        }
    }

    /**
     * 绘制干扰线。
     *
     * @param g
     */
    private void drawLine(Graphics g) {
        for (int i = 0; i < 40; i++) {
            int x = random.nextInt(width);
            int y = random.nextInt(height);
            int xl = random.nextInt(13);
            int yl = random.nextInt(15);
            g.drawLine(x, y, x + xl, y + yl);
        }
    }
}
