package com.GreenBamboo.springwind.codeSystem;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.junit.Test;

/**
 *
 * @author bing <503718696@qq.com>
 */
public class BCodeerTest {
    
    public BCodeerTest() {
    }


    /**
     * Test of getCaptcha method, of class BCodeer.
     */
    @Test
    public void testGetCaptcha() {
        try {
            System.out.println("GetCaptcha");
            BCodeer instance = new BCodeer();
            for (int i = 0; i < 8; i++) {
                ImageCode result = instance.getCaptcha();
                ImageIO.write((RenderedImage) result.getImage(), "png", new File("D:\\codeerTest\\" + result.getCode() + ".png"));
            }

        } catch (IOException ex) {
            Logger.getLogger(BCodeerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
}
