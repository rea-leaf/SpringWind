package com.GreenBamboo.springwind.codeSystem;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.junit.Test;

/**
 * 
 * <pre>
 * 作者：ZSF
 * 项目：SpringWind-CaptchaSystem
 * 类说明：生成算数验证码
 * 日期：2016年5月18日
 * 备注：
 * </pre>
 */
public class FCodeerTest {

	public FCodeerTest() {
	}

	/**
	 * Test of getCaptcha method, of class ECodeer.
	 */
	@Test
	public void testGetCaptcha() {
		try {
			System.out.println("==test getCaptcha()");
			FCodeer instance = new FCodeer();
			
			ImageCode imageCode = instance.getCaptcha();
			ImageIO.write((RenderedImage) imageCode.getImage(), "png",
					new File("D:\\codeerTest\\" + imageCode.getCode() + ".png"));

			Double result = instance.getResult();
			System.out.println(result);

		} catch (IOException ex) {
			Logger.getLogger(FCodeerTest.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
